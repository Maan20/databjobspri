from employee_profile.views import InterviewScheduleForDateView
from oauth2_provider.contrib.rest_framework import TokenHasScope, OAuth2Authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from .serializers import *
from rest_framework import generics, permissions, serializers
from rest_framework.generics import *
from rest_framework.mixins import *
from .Recording import *
from .serializers import *
from jobspri.agoro.RtcTokenBuilderTest import getRTCAgoraToken
import random
from employee_profile.models import InterViewRecording


class HRInterviewsView(
    GenericAPIView, ListModelMixin, RetrieveModelMixin, UpdateModelMixin
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = HRInterviewScheduleSerializer
    queryset = EmployeeInterviewSchedule.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        try:
            return self.queryset.filter(hr_id=self.request.user)
        except:
            return self.queryset

    def get(self, request, id=None):
        if id:
            obj = EmployeeInterviewSchedule.objects.get(id=id)
            obj.agora_token_hr = getRTCAgoraToken(obj.room_id, obj.agora_uid_hr)
            obj.save()
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Status updated successfully",
            }
        )

class EmployeeDetailsView(GenericAPIView, ListModelMixin, RetrieveModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProfileDetailsSerializer
    queryset = User.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        data = HrContact.objects.get(hr_id=self.request.user)
        queryset = self.queryset.filter(
            role=User.EMPLOYEE, id__in=data.cadidate_id.all()
        )
        try:
            if self.request.query_params.get("date"):
                date = self.request.query_params.get("date")
                queryset = queryset.filter(created_at__date=date)
        except:
            pass
        return queryset

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class AssessmentTestView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id=None):
        interview = EmployeeInterviewSchedule.objects.get(id=id)
        tests = (
            AssessmentTestTakers.objects.filter(
                job_id=interview.job_id, user=interview.user_id
            )
            .distinct("assessment_id")
            .order_by("assessment_id", "-created_at")
        )
        res = []
        correct_answers__sum = 0
        total_question__sum = 0
        user_details = {
            "name": interview.user_id.first_name,
            "mobile": interview.user_id.mobile,
            "email": interview.user_id.email,
        }
        for test in tests:
            data = {
                "module_name": test.assessment_id.module_name,
                "correct_answers": test.correct_answers,
                "total_question": test.total_question,
                "wrong_answers": test.total_question - test.correct_answers,
            }
            total_question__sum = total_question__sum + test.total_question
            correct_answers__sum = correct_answers__sum + test.correct_answers

            res.append(data)

        try:
            percentage = (correct_answers__sum / total_question__sum) * 100
        except:
            percentage = 0

        return Response(
            {
                "modules": res,
                "percentage": percentage,
                "total_question": total_question__sum,
                "correct_answers": correct_answers__sum,
                "user_details": user_details,
            }
        )


class StartAgoraRecording(APIView):
    def get(self, request, id=None):

        obj = EmployeeInterviewSchedule.objects.get(id=id)
        token = genrateToken()
        uid = str(random.randint(1000000, 9999999))
        RTCToken = getRTCAgoraToken(obj.room_id, uid)
        response = StartRecording(obj.room_id, uid, token, RTCToken)
        if response["status"] == True:
            obj.agora_resource_id = response["resourceId"]
            obj.agora_start_id = response["sid"]
            obj.agora_uid = uid
            obj.save()
        return Response({"data": response})


class StopAgoraRecording(APIView):
    def get(self, request, id=None):
        obj = EmployeeInterviewSchedule.objects.get(id=id)
        token = genrateToken()
        response = StopRecording(
            obj.room_id, obj.agora_uid, token, obj.agora_resource_id, obj.agora_start_id
        )
        for f in response["files"]:
            i = InterViewRecording()
            i.interview_id = obj
            url = "https://jobspri.s3.ap-south-1.amazonaws.com/" + f["fileName"]
            i.video_url = url
            i.save()

        return Response({"data": response})


class InterviewReview(APIView):
    def post(self, request, id=None):
        data = request.data
        obj = EmployeeInterviewSchedule.objects.get(id=id)
        for review in data["review"]:
            re = EmployeeInterviewRemark()
            re.skill_id = InterviewSkills(review["skill_id"])
            re.marks = review["marks"]
            re.interview_id = obj
            re.save()
        obj.is_selected = data["is_selected"]
        obj.is_call_done = True
        if data["is_selected"] == True:

            obj.status = EmployeeInterviewSchedule.SELECTED
        else:
            obj.status = EmployeeInterviewSchedule.REJECTED
        obj.save()

        return Response({"message": "Remark given successfully"})

    def get(self, request, id=None):
        data = []
        objs = (
            EmployeeInterviewRemark.objects.filter(interview_id=id)
            .distinct("skill_id")
            .order_by("skill_id", "-created_at")
        )
        if objs:
            for i in objs:
                obj = {"skill": i.skill_id.name, "marks": i.marks, "details": i.details}
                data.append(obj)
        i = EmployeeInterviewSchedule.objects.get(id=id)

        user_details = {
            "name": i.user_id.first_name,
            "mobile": i.user_id.mobile,
            "email": i.user_id.email,
        }
        hr_details = {
            "name": i.hr_id.first_name,
            "mobile": i.hr_id.mobile,
            "email": i.hr_id.email,
        }

        return Response(
            {"remark": data, "hr_details": hr_details, "user_details": user_details}
        )


class EmployeeContactListView(GenericAPIView, ListModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeContactListSerializer
    queryset = EmployeeContactList.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        queryset = self.queryset
        user_id = self.kwargs["user_id"]
        queryset = queryset.filter(user_id=user_id)
        return queryset

    def get(self, request, user_id=None):
        return self.list(request)


class EmployeeProfileProgress(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, user_id=None):
        profile_complate = False
        applied_for_job = False
        Interview_scheduled = False
        data= {
            "profile_complate":profile_complate,
            "applied_for_job": applied_for_job,
            "Interview_scheduled":Interview_scheduled
        }
        profile_complate = EmployeePriviousWorkExperience.objects.filter(
            user_id=user_id
        ).exists()
        if profile_complate:
            profile_complate = EmployeeEducation.objects.filter(
                user_id=user_id
            ).exists()
        if profile_complate:
            profile_complate = EmployeeInterest.objects.filter(
                user_id=user_id
            ).exists()
        if profile_complate:
            profile_complate = EmployeeJobPreferences.objects.filter(
                user_id=user_id
            ).exists()
        if profile_complate:
            profile_complate = EmployeeProjects.objects.filter(
                user_id=user_id
            ).exists()
        if profile_complate:
            profile_complate = EmployeeSkills.objects.filter(
                user_id=user_id
            ).exists()
        data['profile_complate']=profile_complate
        return Response(data)


class CandidateView(
    generics.GenericAPIView,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializer
    lookup_field = "id"
    method_serializer_classes = {
        ("POST"): CandidateListSerializers,
        ("GET","PUT" ,"DELETE"): CandidateSerializer,
    }
 
    def get_queryset(self):
        queryset = self.queryset
        hr_id = self.kwargs["hr_id"]
        if hr_id != "b863ebd6-7979-4cf2-ba60-b032f77900b3":
        # if hr_id != "f07ff7d6-7e75-479c-b61a-a77d6355658f":
            queryset = queryset.filter(hr_id=hr_id)
        return queryset

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()

        if "data" in kwargs:
            data = kwargs["data"]

            if isinstance(data, list):
                kwargs["many"] = True

        return serializer_class(*args, **kwargs)

    def get(self, request, id=None, hr_id=None):
        if id == None:
            return self.list(request, id)
        else:
            return self.retrieve(request, id)

    def post(self, request,hr_id=None):
        self.create(request)
        return Response(
            {
                "message": "Created successfully",
            }
        )

    def put(self,  request, id=None,hr_id=None):
        self.update(request, id)
        return Response(
            {
                "message": "Updated successfully",
            }
        )

    def delete(self, request, id=None,hr_id=None):
        self.delete(request, id)
        return Response(
            {
                "message": "Deleted successfully",
            }
        )

class HrListView(GenericAPIView, ListModelMixin, RetrieveModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer
    queryset = User.objects.all()
    lookup_field = "id"

    def get_queryset(self):
       
        queryset = self.queryset.filter(
            role=User.HR )
        
        return queryset

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)