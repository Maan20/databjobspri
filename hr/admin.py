from django.contrib import admin
from employee_profile.models import *
from .models import *

class CandidateAdmin(admin.ModelAdmin):
    search_fields = ["id", "name"]
    list_display = ("id", "name")

admin.site.register(Candidate, CandidateAdmin)
