from rest_framework import serializers, exceptions
from accounts.models import *
from company.models import *
from company.serializers import *
from .models import *
from accounts.serializers import UserSerializer
from employee_profile.serializers import *
from django.db.models import Sum

class CandidateListSerializers(serializers.ListSerializer):
   
    def create(self, validated_data):
        log_levels = [Candidate(**item) for item in validated_data]
        candidate = Candidate.objects.bulk_create(log_levels)
        return candidate


class CandidateSerializer(serializers.ModelSerializer):
    hr_name = serializers.ReadOnlyField(source="hr_id.first_name")
    class Meta:
        model = Candidate
        fields = "__all__"
        list_serializer_class = CandidateListSerializers


class HRInterviewScheduleSerializer(serializers.ModelSerializer):
    interview_recording = InterViewRecordingSerializer(read_only=True, many=True)
    employee_details = serializers.SerializerMethodField(
        "get_employee_details", read_only=True
    )
    job_details = serializers.SerializerMethodField("get_job_details", read_only=True)
    assessment_details = serializers.SerializerMethodField(
        "get_assessment_details", read_only=True
    )
    time_slot_details = serializers.SerializerMethodField(
        "get_time_slot_details", read_only=True
    )

    def get_time_slot_details(self, obj):
        slot = InterviewTimeSlot.objects.get(id=obj.time_slot.id)
        return slot.time

    def get_employee_details(self, obj):
        details = ProfileSerializer(obj.user_id).data
        personal_details = UserSerializer(obj.user_id).data
        return {"profile_details": details, "personal_details": personal_details}

    def get_job_details(self, obj):
        return JobSerializer(obj.job_id).data

    def get_assessment_details(self, obj):
        tests = (
            AssessmentTestTakers.objects.filter(job_id=obj.job_id, user=obj.user_id)
            .distinct("assessment_id")
            .order_by("assessment_id", "-created_at")
        )
        correct_answers__sum = 0
        total_question__sum = 0
        for test in tests:
            total_question__sum = total_question__sum + test.total_question
            correct_answers__sum = correct_answers__sum + test.correct_answers

        try:
            percentage = (
                correct_answers__sum/ total_question__sum
            ) * 100
            return percentage
        except:
            return 0

    class Meta:
        model = EmployeeInterviewSchedule
        fields = "__all__"
