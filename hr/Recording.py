import requests
import time
from django.conf import settings
import base64
import http.client

TIMEOUT = 60

# TODO: Fill in AppId, Basic Auth string, Cname (channel name), and cloud storage information
APPID = settings.AGORA_APP_ID
appCertificate = settings.AGORA_APP_CERTIFICATE
Cname = ""
ACCESS_KEY = settings.AWS_ACCESS_KEY_ID
SECRET_KEY = settings.AWS_SECRET_ACCESS_KEY
VENDOR = 1
REGION = 14
BUCKET = settings.AWS_STORAGE_BUCKET_NAME


url = "https://api.agora.io/v1/apps/%s/cloud_recording/" % APPID

layoutConfig = [
    {
        "x_axis": 0.0,
        "y_axis": 0.0,
        "width": 0.3,
        "height": 0.3,
        "alpha": 0.9,
        "render_mode": 1,
    },
    {
        "x_axis": 0.3,
        "y_axis": 0.0,
        "width": 0.3,
        "height": 0.3,
        "alpha": 0.77,
        "render_mode": 0,
    },
]


def cloud_post(Auth, url, data=None, timeout=TIMEOUT):
    headers = {"Content-type": "application/json", "Authorization": Auth}

    try:
        response = requests.post(
            url, json=data, headers=headers, timeout=timeout, verify=False
        )
        print(
            "url: %s, request body:%s response: %s"
            % (url, response.request.body, response.json())
        )
        return response
    except requests.exceptions.ConnectTimeout:
        raise Exception("CONNECTION_TIMEOUT")
    except requests.exceptions.ConnectionError:
        raise Exception("CONNECTION_ERROR")


def cloud_get(Auth, url, timeout=TIMEOUT):
    headers = {"Content-type": "application/json", "Authorization": Auth}
    try:
        response = requests.get(url, headers=headers, timeout=timeout, verify=False)
        print(
            "url: %s,request:%s response: %s"
            % (url, response.request.body, response.json())
        )
        return response
    except requests.exceptions.ConnectTimeout:
        raise Exception("CONNECTION_TIMEOUT")
    except requests.exceptions.ConnectionError:
        raise Exception("CONNECTION_ERROR")


def genrateToken():
    # Concatenate customer key and customer secret and use base64 to encode the concatenated string
    credentials = settings.AGORA_CUSTOMER_KEY + ":" + settings.AGORA_CUSTOMER_SECRET
    # Encode with base64
    base64_credentials = base64.b64encode(credentials.encode("utf8"))
    credential = base64_credentials.decode("utf8")

    auth = "basic " + credential

    return auth


def StartRecording(Cname, uid, Auth, RTCToken):
    acquire_body = {"uid": uid, "cname": Cname, "clientRequest": {}}

    # Set up two regions for two users
    start_body = {
        "uid": uid,
        "cname": Cname,
        "clientRequest": {
            "token": RTCToken,
            "storageConfig": {
                "secretKey": SECRET_KEY,
                "region": REGION,
                "accessKey": ACCESS_KEY,
                "bucket": BUCKET,
                "vendor": VENDOR,
            },
            "recordingFileConfig": {"avFileType": ["hls", "mp4"]},
            "recordingConfig": {
                "audioProfile": 0,
                "channelType": 0,
                "maxIdleTime": 30,
                "transcodingConfig": {
                    "width": 640,
                    "height": 360,
                    "fps": 15,
                    "bitrate": 600,
                    "mixedVideoLayout": 1,
                    "backgroundColor": "#fff000",
                    # "layoutConfig": layoutConfig,
                },
            },
        },
    }
    acquire_url = url + "acquire"
    r_acquire = cloud_post(Auth, acquire_url, acquire_body)
    if r_acquire.status_code == 200:
        resourceId = r_acquire.json()["resourceId"]
    else:
        print(
            "Acquire error! Code: %s Info: %s"
            % (r_acquire.status_code, r_acquire.json())
        )
        return {"status": False, "message": r_acquire.json()}

    start_url = url + "resourceid/%s/mode/mix/start" % resourceId
    r_start = cloud_post(Auth, start_url, start_body)
    if r_start.status_code == 200:
        # sid = r_start.json()["sid"]
        # time.sleep(30)
        # query_url = url + "resourceid/%s/sid/%s/mode/mix/query" % (resourceId, sid)
        # r_query = cloud_get(Auth, query_url)
        return {
            "status": True,
            "message": r_start.json(),
            "sid": r_start.json()["sid"],
            "resourceId": resourceId,
        }
    else:
        return {"status": False, "message": r_start.json()}


def StopRecording(Cname, uid, Auth, resourceId, sid):
    stop_body = {"uid": uid, "cname": Cname, "clientRequest": {}}
    stop_url = url + "resourceid/%s/sid/%s/mode/mix/stop" % (resourceId, sid)
    r_stop = cloud_post(Auth, stop_url, stop_body)
    return {"status": r_stop.status_code, "message": r_stop.json(),"files":r_stop.json()["serverResponse"]["fileList"]}
