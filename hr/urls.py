from django.conf.urls import url
from django.urls import path
from oauth2_provider import views as oauth2_views
from .views import *

urlpatterns = [
    path("interviews", HRInterviewsView.as_view()),
    path("interview/<id>/assessment-details", AssessmentTestView.as_view()),
    path("interview/<id>", HRInterviewsView.as_view()),
    path("interview/review/<id>", InterviewReview.as_view()),
    path("employee/profile/contact-list/<user_id>", EmployeeContactListView.as_view()),
    path("employee/profile/progress/<user_id>", EmployeeProfileProgress.as_view()),

    path("employee/profile/<id>", EmployeeDetailsView.as_view()),
    path("employee/profile", EmployeeDetailsView.as_view()),
    path("start-recording/<id>", StartAgoraRecording.as_view()),
    path("stop-recording/<id>", StopAgoraRecording.as_view()),
    path("<hr_id>/candidate/<id>", CandidateView.as_view()),
    path("<hr_id>/candidate", CandidateView.as_view()),
    path("list", HrListView.as_view()),
    path("details/<id>", HrListView.as_view()),

]