from accounts.models import User
from django.db import models
import uuid

# Create your models here.

class Candidate(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False,unique=True)
    name = models.CharField(max_length=100, null=True, blank=False)
    mobile= models.CharField(max_length=50,null=True,blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True,default="")
    interview = models.CharField(max_length=50, null=True, blank=True,default="")
    action = models.CharField(max_length=50, null=True, blank=True,default="")
    review = models.TextField(null=True,blank=True)
    hr_id = models.ForeignKey(User,null=True,blank=True, related_name="hr_candidates",on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
