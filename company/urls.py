from django.conf.urls import url
from django.urls import path
from company.views import *

urlpatterns = [
    path("company/signup/",CompanyView.as_view()),
    path("company/", CompanyView.as_view()),
    path("jobs/", JobView.as_view()),
    path("jobs/recommended", JobView.as_view()),
    path("job/<id>", JobView.as_view()),
    path("job/<id>/requirement-questions",JobRequirementQuestionsView.as_view()),
    path("job/<job_id>/assessment-modules",JobAssessmentModuleView.as_view()),
    path("job/<id>/assessment-video",JobAssessmentVideoView.as_view()),
    path("job/<id>/assessment-details",JobAssessmentDetailsView.as_view()),
    path("job/application/",JobApplicationView.as_view()),
    path("job/assessment-test/",JobAssessmentTestView.as_view()),
    path("job/assessment-test/<id>/report",JobAssessmentTestResult.as_view()),
    path("get-dates",DatesForInterviews.as_view())
]