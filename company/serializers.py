from rest_framework import serializers
from .models import *
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from rest_framework import exceptions
from jobspri_admin.serializers import SkillsSerializer, JobRequirementQuestionSerializer
from employee_profile.models import *


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = "__all__"


class JobLocationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobLocation
        fields = ["location"]


class JobSerializer(serializers.ModelSerializer):
    company_details = CompanySerializer(source="company_id")
    job_location = JobLocationsSerializer(many=True)
    skills = SkillsSerializer(many=True)
    is_applied = serializers.SerializerMethodField("check_if_applied", read_only=True)
    all_modules_clear = serializers.SerializerMethodField(
        "check_all_modules_clear", read_only=True
    )
    modules_clear_percent = serializers.SerializerMethodField(
        "check_modules_clear_percent", read_only=True
    )

    def check_all_modules_clear(self, obj):
        return False

    def check_modules_clear_percent(self, obj):
        percent = 0
        count = 0
        try:
            is_applied = JobApplication.objects.filter(
                job_id=obj.id, user_id=self.context["request"].user
            ).exists()
            if is_applied:
                assessments = JobAssessmentModule.objects.filter(job_id=obj.id)
                for ass in assessments:
                    is_exists = AssessmentTestTakers.objects.filter(
                        assessment_id=obj.id,
                        user=self.context["request"].user,
                        is_assessment_passed=True,
                    ).exists()
                count = count + 1
                total = assessments.count()
                percent = int((count / total) * 100)
            return percent

        except:
            return 0

    is_interview_schedule = serializers.SerializerMethodField(
        "check_if_interview_schedule", read_only=True
    )

    def check_if_applied(self, obj):
        try:
            is_applied = JobApplication.objects.filter(
                job_id=obj.id, user_id=self.context["request"].user
            ).exists()
            return is_applied
        except:
            return False

    def check_if_interview_schedule(self, obj):
        try:
            is_interview_schedule = EmployeeInterviewSchedule.objects.filter(
                job_id=obj.id, user_id=self.context["request"].user
            ).exists()
            return is_interview_schedule
        except:
            return False

    class Meta:
        model = Job
        exclude = ("requirements_questions",)


class JobRequirementQuestionsSerializer(serializers.ModelSerializer):
    requirements_questions = JobRequirementQuestionSerializer(many=True)

    class Meta:
        model = Job
        fields = ("requirements_questions",)


class JobAssessmentVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobAssessmentModule
        fields = ("video",)


class JobAssessmentModuleListSerializer(serializers.ModelSerializer):
    is_viewed = serializers.SerializerMethodField("check_is_viewed", read_only=True)
    is_assessment_passed = serializers.SerializerMethodField(
        "check_is_assessment_passed", read_only=True
    )
    assessment_marks = serializers.SerializerMethodField(
        "get_assessment_marks", read_only=True
    )

    def check_is_viewed(self, obj):
        try:
            is_applied = AssessmentTestTakers.objects.filter(
                assessment_id=obj.id, user=self.context["request"].user
            ).exists()
            return is_applied
        except:
            return False

    def check_is_assessment_passed(self, obj):
        try:
            is_applied = AssessmentTestTakers.objects.filter(
                assessment_id=obj.id,
                user=self.context["request"].user,
                is_assessment_passed=True,
            ).exists()
            return is_applied
        except:
            return False

    def get_assessment_marks(self, obj):
        
        object = AssessmentTestTakers.objects.filter(
            assessment_id=obj.id,
            user=self.context["request"].user,
            is_assessment_passed=True,
        ).first()
        if object:
            percentage = round((object.correct_answers / object.total_question) * 100,2)
            return percentage
        else:
            return 0
        

    class Meta:
        model = JobAssessmentModule
        fields = ("id", "module_name", "is_viewed", "is_assessment_passed", "order", "assessment_marks")


class JobAssessmentQuestionsAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobAssessmentQuestionsAnswer
        exclude = ("question",)


class JobAssessmentQuestionSerializer(serializers.ModelSerializer):
    questions_options = JobAssessmentQuestionsAnswerSerializer(many=True)

    class Meta:
        model = JobAssessmentQuestion
        fields = "__all__"


class JobAssessmentDetailsSerializer(serializers.ModelSerializer):
    job_question = JobAssessmentQuestionSerializer(many=True)

    class Meta:
        model = JobAssessmentModule
        fields = "__all__"


class JobApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobApplication
        fields = ("job_id",)


class JobAssessmentTestSerializer(serializers.Serializer):
    question_data = serializers.JSONField()
    job_id = serializers.UUIDField(format="hex_verbose")
    assessment_id = serializers.UUIDField(format="hex_verbose")

    def validate(self, data):
        request = self.context.get("request")
        print(request)
        return data


class AssessmentTestResponseSerializer(serializers.ModelSerializer):
    options = serializers.SerializerMethodField("get_guestions_options", read_only=True)

    class Meta:
        model = AssessmentTestResponse
        exclude = ("quiztaker", "id")
        depth = 1

    def get_guestions_options(self, obj):
        options = JobAssessmentQuestionsAnswer.objects.filter(question=obj.question)
        return JobAssessmentQuestionsAnswerSerializer(options, many=True).data


class AssessmentTestTakersSerializer(serializers.ModelSerializer):
    question_set = AssessmentTestResponseSerializer(many=True)

    class Meta:
        model = AssessmentTestTakers
        fields = "__all__"