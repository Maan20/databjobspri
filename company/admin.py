from django.contrib import admin
from company.models import *
import nested_admin



class CompanyAdmin(admin.ModelAdmin):
    search_fields = ['id','name']
    list_display = ('id','name')

class JobAssessmentQuestionsAnswerInline(nested_admin.NestedTabularInline):
    model = JobAssessmentQuestionsAnswer
    max_num = 4
    extra =  4


class JobAssessmentQuestionInline(nested_admin.NestedTabularInline):
    model = JobAssessmentQuestion
    inlines = [JobAssessmentQuestionsAnswerInline,]
    extra = 0

class JobAssessmentInline(nested_admin.NestedModelAdmin):
    model = JobAssessmentModule
    list_display = ('id','job_id','module_name')
    inlines = [JobAssessmentQuestionInline,]

class JobLocationInline(nested_admin.NestedTabularInline):
    model = JobLocation
  
class JobAdmin(nested_admin.NestedModelAdmin):
    model = Job
    inlines = [JobLocationInline,]
    search_fields = ['id','title']
    list_display = ('title','company_id','loctions')
    def loctions(self, obj):
        return ",".join([l.location for l in obj.job_location.all()])


class AssessmentTestResponseInline(nested_admin.NestedTabularInline):
    model = AssessmentTestResponse

class AssessmentTestTakersAdmin(nested_admin.NestedModelAdmin):
    list_display = ('job_id','user','assessment_id')
    inlines = [AssessmentTestResponseInline,]
    model = AssessmentTestTakers

class JobApplicationAdmin(admin.ModelAdmin):
    list_display = ('job_id','user_id')


admin.site.register(Company,CompanyAdmin)
admin.site.register(JobApplication,JobApplicationAdmin)

admin.site.register(Job,JobAdmin)
admin.site.register(JobAssessmentModule,JobAssessmentInline)
admin.site.register(AssessmentTestTakers,AssessmentTestTakersAdmin)


