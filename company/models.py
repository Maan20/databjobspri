from django.db import models
import uuid
from jobspri_admin.models import *
from accounts.models import *
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from fcm_django.models import FCMDevice

class Company(models.Model):
    id          = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name        = models.CharField(max_length=500, null=True, blank=False)
    image       = models.ImageField(null=True, blank=True,upload_to="company/%Y/%m/%d/")
    location    = models.TextField(null=True,blank=True)
    description = models.TextField(null=True,blank=True)
    email       = models.EmailField(null=True,blank=True)
    url         = models.TextField(null=True,blank=True)
    phone       = models.TextField(null=True,blank=True)
    created_at  = models.DateTimeField(auto_now_add=True, null=True)
    updated_at  = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class Job(models.Model):
    id          = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company_id  = models.ForeignKey(Company, on_delete=models.CASCADE, null=True, related_name="company_job")
    position    = models.CharField(max_length=500, null=True, blank=False)
    min_experience  = models.CharField(max_length=500, null=True, blank=False)
    max_experience  = models.CharField(max_length=500, null=True, blank=False)
    department      = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True, related_name="department_job")
    salary_in_lpa   = models.DecimalField(decimal_places=2, max_digits=25, null=True, blank=False)
    job_type    = models.CharField(max_length=500, null=True, blank=False)
    job_time_type = models.CharField(max_length=500, null=True, blank=False)
    title       = models.CharField(max_length=500, null=True, blank=False)
    description = models.TextField(null=True, blank=False)
    requirements = models.TextField(null=True, blank=False)
    eligibility       = models.TextField(null=True, blank=False)
    skills      = models.ManyToManyField(Skill,blank=True)
    requirements_questions = models.ManyToManyField(JobRequirementQuestion,blank=True)
    is_open = models.BooleanField(default=True)
    created_at  = models.DateTimeField(auto_now_add=True, null=True)
    updated_at  = models.DateTimeField(auto_now=True, null=True)    
    def __str__(self):
        return self.title + "("+ self.company_id.name +")"

#@receiver(post_save, sender=Job)
# def handle_new_job(sender, **kwargs):
#     job = kwargs.get('instance')
#     title = "Job Alert"
#     description = "A new job has been posted that seems to be matching your skill or interest. Do check it out for further opportunities. "
#     devices = FCMDevice.objects.filter(user__role=User.EMPLOYEE)
#     res = devices.send_message(title=title, body=description, data={"test": "test"})
#     notification = Notification.objects.create(title=title,description=description,type=Notification.JOB)
#     objs = [
#         UserNotification(
#             user_id=e.user,
#             notification_id=notification
#             )
#         for e in devices 
#         ]
#     msg = UserNotification.objects.bulk_create(objs)
    
    
class JobLocation(models.Model):
    id    = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job_id  = models.ForeignKey(Job, on_delete=models.CASCADE,related_name="job_location")
    location = models.TextField(null=True, blank=False)
    def __str__(self):
        return self.location 


class JobAssessmentModule(models.Model):
    id    = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job_id  = models.ForeignKey(Job, on_delete=models.CASCADE,related_name="job_assessment")
    module_name = models.CharField(max_length=500, null=False, blank=False)
    video = models.FileField(null=True, blank=False,upload_to="assessment_video/%Y/%m/%d/")
    min_marks = models.IntegerField(help_text=_("Enter Minimum marks in percentage"),)
    no_of_questions_in_test = models.IntegerField(default=1)
    order = models.IntegerField(default=1)
    duration = models.CharField(null=True, max_length=100,blank=True,default=30,help_text=_("Enter duration in minutes example 60"),)
    created_at  = models.DateTimeField(auto_now_add=True, null=True)
    updated_at  = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.module_name    


class JobAssessmentQuestion(models.Model):
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    assessment_id   = models.ForeignKey(JobAssessmentModule, on_delete=models.CASCADE,related_name="job_question")
    question        = models.TextField()
    remark          = models.TextField(null=True,blank=True)


    def __str__(self):
        return self.question


class JobAssessmentQuestionsAnswer(models.Model):
    id    = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    question = models.ForeignKey(JobAssessmentQuestion, on_delete=models.CASCADE,related_name="questions_options")
    option = models.CharField(max_length=1000)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.option


class JobApplication(models.Model):
    id    = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job_id  = models.ForeignKey(Job, on_delete=models.CASCADE,related_name="job_application")
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name="employee_job_application")
    created_at  = models.DateTimeField(auto_now_add=True, null=True)
    updated_at  = models.DateTimeField(auto_now=True, null=True)


class AssessmentTestTakers(models.Model):
    id    = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    job_id = models.ForeignKey(Job, on_delete=models.CASCADE)
    assessment_id = models.ForeignKey(JobAssessmentModule, on_delete=models.CASCADE)
    correct_answers = models.IntegerField(default=0)
    total_question = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    is_assessment_passed =  models.BooleanField(default=False)
    created_at  = models.DateTimeField(auto_now_add=True, null=True)
    updated_at  = models.DateTimeField(auto_now=True, null=True)


class AssessmentTestResponse(models.Model):
    id    = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    quiztaker = models.ForeignKey(AssessmentTestTakers, on_delete=models.CASCADE,related_name="question_set")
    question = models.ForeignKey(JobAssessmentQuestion, on_delete=models.CASCADE,related_name="question_set")
    answer = models.ForeignKey(JobAssessmentQuestionsAnswer,on_delete=models.CASCADE,null=True,blank=True)


