from oauth2_provider.contrib.rest_framework import TokenHasScope, OAuth2Authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions, generics, serializers
from .serializers import *
from jobspri_admin.serializers import JobRequirementQuestionSerializer
from rest_framework.generics import *
from rest_framework.mixins import *
from .models import *
from datetime import datetime, timedelta
from django.db.models import Q


class CompanyView(GenericAPIView, CreateModelMixin, ListModelMixin, RetrieveModelMixin):
    # authentication_classes = [OAuth2Authentication]
    # permission_classes = [IsAuthenticated]
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    lookup_field = "id"

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Thank you for your registation with jobspri",
            }
        )

    def perform_create(self, serializer):
        serializer.save()

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class JobView(
    GenericAPIView,
    ListModelMixin,
    CreateModelMixin,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobSerializer
    queryset = Job.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        queryset = self.queryset
        if self.request.query_params.get("search"):
            q = self.request.query_params.get("search")
            queryset = queryset.filter(
                Q(job_type__icontains=q)
                | Q(job_time_type__icontains=q)
                | Q(title__icontains=q)
                | Q(description__icontains=q)
                | Q(eligibility__icontains=q)
                | Q(skills__name__icontains=q)
                | Q(job_location__location__icontains=q)
            ).distinct()
        if self.request.query_params.get("job_type"):
            job_type =self.request.query_params.get("job_type")
            queryset = queryset.filter(job_type=job_type)
        if self.request.query_params.get("skills"):
            skills =self.request.query_params.get("skills")
            queryset = queryset.filter(skills__name=skills)
        if self.request.query_params.get("location"):
            location =self.request.query_params.get("location")
            queryset = queryset.filter(job_location__location=location)
        if self.request.query_params.get("applied"):
            queryset = queryset.filter(
                job_application__user_id=self.request.user
            ).exclude(employeeinterviewschedule__user_id=self.request.user)

        return queryset

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class JobRequirementQuestionsView(
    GenericAPIView,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobRequirementQuestionsSerializer
    queryset = Job.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class JobAssessmentModuleView(
    GenericAPIView,
    ListModelMixin,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobAssessmentModuleListSerializer
    queryset = JobAssessmentModule.objects.all()
    lookup_field = "job_id"

    def get_queryset(self):
        queryset = self.queryset
        job_id = self.kwargs["job_id"]
        queryset = queryset.filter(job_id=job_id).order_by("order")
        return queryset

    def get(self, request, job_id=None):
        return self.list(request)


class JobAssessmentVideoView(
    GenericAPIView,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobAssessmentVideoSerializer
    queryset = JobAssessmentModule.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        return self.retrieve(request, id)


class JobAssessmentDetailsView(
    GenericAPIView,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobAssessmentDetailsSerializer
    queryset = JobAssessmentModule.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        return self.retrieve(request, id)


class JobApplicationView(
    GenericAPIView,
    CreateModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobApplicationSerializer

    def post(self, request):
        self.create(request)
        data = Job.objects.filter(
            job_application__user_id=self.request.user
        ).exclude(employeeinterviewschedule__user_id=self.request.user)
        serializer = JobSerializer(data,many=True)
        return Response(
            {
                "applied_jobs":serializer.data
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)


class JobAssessmentTestView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = JobApplicationSerializer

    def post(self, request):
        serializer = JobAssessmentTestSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        data = request.data

        correct_answers = 0
        percentage = 0
        result = False
        test = AssessmentTestTakers.objects.create(
            job_id=Job(data["job_id"]),
            assessment_id=JobAssessmentModule(data["assessment_id"]),
            user=request.user,
        )

        for i in data["question_data"]:
            is_correct = JobAssessmentQuestionsAnswer.objects.filter(
                id=i["answer"], is_correct=True
            ).count()
            correct_answers += is_correct
            AssessmentTestResponse.objects.create(
                quiztaker=test,
                question=JobAssessmentQuestion(i["questions"]),
                answer=JobAssessmentQuestionsAnswer(i["answer"]),
            )

        assessment = JobAssessmentModule.objects.get(id=data["assessment_id"])
        total_question = assessment.job_question.count()

        min_marks = int(assessment.min_marks)

        if correct_answers > 0:
            percentage = int((correct_answers * 100) / total_question)

        if percentage >= min_marks:
            result = True

        test.correct_answers = correct_answers
        test.completed = True
        test.is_assessment_passed = result
        test.total_question = total_question
        test.save()

        return Response(
            {
                "id": test.id,
                "message": "Test submitted successfully",
                "correct_answers": correct_answers,
                "assessment_passed": result,
                "total_question": total_question,
                "wrong_answers": total_question - correct_answers,
                "percentage": percentage,
                "min_marks": min_marks,
            }
        )


class JobAssessmentTestResult(
    GenericAPIView,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = AssessmentTestTakersSerializer
    queryset = AssessmentTestTakers.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        return self.retrieve(request, id)


class DatesForInterviews(APIView):
    def get(self, request, id=None):
        today = datetime.today()
        dates = []
        dates.append(today.strftime("%Y-%m-%d"))

        for i in range(3):
            today = today + timedelta(days=1)
            dates.append(today.strftime("%Y-%m-%d"))

        return Response(dates)