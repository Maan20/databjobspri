#! /usr/bin/python
# ! -*- coding: utf-8 -*-

import sys
import os
import time
from random import randint
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from jobspri.agoro.RtcTokenBuilder import RtcTokenBuilder,Role_Attendee

from django.conf import settings

appID = settings.AGORA_APP_ID
appCertificate = settings.AGORA_APP_CERTIFICATE

def getRTCAgoraToken(channelName,uid):
    
    expireTimeInSeconds = 860000
    currentTimestamp = int(time.time())
    privilegeExpiredTs = currentTimestamp + expireTimeInSeconds

    token = RtcTokenBuilder.buildTokenWithUid(appID, appCertificate, channelName, uid, Role_Attendee, privilegeExpiredTs)
    print("Token with int uid: {}".format(token))
    return token
    
def getAccessToken(channelName, uid):
    token = AccessToken(appId, appCertificate, channelName, uid)
    return token

