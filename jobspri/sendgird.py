import json
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (
    Mail,
    CustomArg,
    Attachment,
    FileContent,
    FileName,
    FileType,
    Disposition,
    ContentId,
)
from django.conf import settings
from python_http_client import exceptions
import requests
import base64

sg = SendGridAPIClient(settings.SENDGRID_API_KEY)


def EmailWithDynamicData(
    template_id,
    recievers_data,
    dynamic_data,
    unique_id=None,
    from_email="JobSpri <info@jobspri.com>",
    attachment=False,
):
    message = Mail(from_email=from_email, to_emails=recievers_data,)
    message.dynamic_template_data = dynamic_data
    message.template_id = template_id
    message.custom_arg = [
        CustomArg("unique_id", unique_id),
    ]
    if attachment:
        file_path = settings.ATTACHMENT_FILE_PATH
        with open(file_path, "rb") as f:
            data = f.read()
            f.close()
        encoded = base64.b64encode(data).decode()
        attachment = Attachment()
        attachment.file_content = FileContent(encoded)
        attachment.file_type = FileType("application/pdf")
        attachment.file_name = FileName(
            "Your-work-place-pension-questions-answered.pdf"
        )
        attachment.disposition = Disposition("attachment")
        message.attachment = attachment
    try:
        response = sg.send(message)
        # print(response.headers)
        return response.headers.get("X-Message-Id")
    except Exception as e:
        print(e)


def CreateSingleRecipient(data):
    response = sg.client.contactdb.recipients.post(request_body=data)
    responseJson = json.loads(response.body.decode("utf-8"))
    persistedRecipients = responseJson.get("persisted_recipients")
    return persistedRecipients[0]


def AddReciepientToList(list_id, recipient_id):
    try:
        response = (
            sg.client.contactdb.lists._(list_id).recipients._(recipient_id).post()
        )
    except exceptions.BadRequestsError as e:
        print(e.body)


def AddReciepientToNewMarketingList(recipient_details):
    try:
        url = "https://api.sendgrid.com/v3/marketing/contacts"
        payload = {"list_ids": ['309b4e56-2b42-4d02-80a6-ca570c492ff2'], "contacts": recipient_details}
        headers = {
            "authorization": "Bearer " + settings.SENDGRID_API_KEY,
            "Content-Type": "application/json",
        }
        response = requests.request(
            "PUT", url, data=json.dumps(payload), headers=headers
        )
        return response
    except exceptions.BadRequestsError as e:
        print(e.body)


def GetEmailActivityByUniqueId(unique_id):
    try:
        headers = {"authorization": "Bearer " + settings.SENDGRID_API_KEY}
        payload = "{}"
        url = (
            "https://api.sendgrid.com/v3/messages?limit=1&query=unique_args%5B'unique_id'%5D%3D'"
            + unique_id
            + "'"
        )
        response = requests.request("GET", url, data=payload, headers=headers)
        return json.loads(response.text)
    except exceptions.BadRequestsError as e:
        print(e.body)
