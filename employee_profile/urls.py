from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    path("profile/<user_id>", EmployeeProfileView.as_view()),
    path("profile/", EmployeeProfileView.as_view()),
    path("profile/picture/<user_id>", EmployeeProfilePictureView.as_view()),
    path("profile/picture/", EmployeeProfilePictureView.as_view()),
    path("time-slot", InterviewScheduleForDateView.as_view()),
    path("schedule-interview/", EmployeeInterviewScheduleView.as_view()),
    path("schedule-interview/details/<id>/", EmployeeInterviewScheduleView.as_view()),
    path("schedule-interview/create/", EmployeeInterviewScheduleCreateView.as_view()),
    # Work experience
    path("profile/work-experience/", PriviousWorkExperienceView.as_view()),
    path("profile/work-experience/<id>/", PriviousWorkExperienceView.as_view()),
    # education
    path("profile/education/", EmployeeEducationView.as_view()),
    path("profile/education/<id>/", EmployeeEducationView.as_view()),
    # project
    path("profile/project/", EmployeeProjectsView.as_view()),
    path("profile/project/<id>/", EmployeeProjectsView.as_view()),
    # job-preference
    path("profile/job-preference/", EmployeeJobPreferencesView.as_view()),
    path("profile/job-preference/<id>/", EmployeeJobPreferencesView.as_view()),
    # Achievement
    path("profile/achievement/", EmployeeAchievementsView.as_view()),
    path("profile/achievement/<id>/", EmployeeAchievementsView.as_view()),
    # Certification
    path("profile/certification/", EmployeeCertificationView.as_view()),
    path("profile/certification/<id>/", EmployeeCertificationView.as_view()),
    # Skill
    path("profile/skill/", EmployeeSkillsView.as_view()),
    path("profile/skill/", EmployeeSkillsView.as_view()),
    path("profile/bulk/skills/", EmployeeBulkSkillsView.as_view()),

    path("profile/skill/<id>/", EmployeeSkillsView.as_view()),
    # Interest
    path("profile/interest/", EmployeeInterestView.as_view()),
    path("profile/interest/<id>/", EmployeeInterestView.as_view()),
    # reference
    path("profile/reference/", EmployeeReferenceView.as_view()),
    path("profile/reference/<id>/", EmployeeReferenceView.as_view()),
    #Notification
    path("notification/", UserNotificationView.as_view()),
    path("contact-list/", EmployeeContactListView.as_view()),
    path("contact-list/check", EmployeeContactListCheckView.as_view()),

]