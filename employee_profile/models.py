from django.db import models
from accounts.models import *
from jobspri_admin.models import *
from company.models import Job
import uuid


class EmployeeProfile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    gender = models.CharField(max_length=10, null=True, blank=True, default=" ")
    user_id = models.OneToOneField(
        User, on_delete=models.CASCADE, null=True, related_name="employee_profile"
    )
    profile_picture = models.ImageField(
        null=True, blank=True, upload_to="profile_pictures/%Y/%m/%d/"
    )
    summary = models.TextField(null=True, blank=True, default=" ")
    linkedin_profile_url = models.CharField(
        max_length=500, null=True, blank=True, default=" "
    )
    total_work_experience = models.CharField(
        max_length=500, null=True, blank=True, default=" "
    )
    current_location = models.CharField(
        max_length=500, null=True, blank=True, default=" "
    )
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def fullname(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        return str(self.id)


class EmployeePriviousWorkExperience(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        related_name="employee_work_experience",
    )
    company_name = models.CharField(max_length=500, null=True, blank=True)
    designation = models.CharField(max_length=500, null=True, blank=True, default=" ")
    is_current_company = models.BooleanField(
        default=False,
    )
    started_working_wrom = models.DateField(null=True, blank=True, default=" ")
    description = models.TextField(null=True, blank=True, default=" ")
    worked_till = models.DateField(null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeEducation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_eductaion"
    )
    course = models.ForeignKey(
        Course, on_delete=models.SET_NULL, null=True, default=" "
    )
    specialization = models.ForeignKey(
        Specialization, on_delete=models.SET_NULL, null=True
    )
    university = models.CharField(max_length=500, null=True, blank=True, default=" ")
    type = models.CharField(max_length=500, null=True, blank=True, default=" ")
    start_date = models.DateField(null=True, blank=True, default=" ")
    end_date = models.DateField(null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeAchievements(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_achievement"
    )
    title = models.CharField(max_length=500, null=True, blank=True, default=" ")
    description = models.TextField(null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeInterest(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_interest"
    )
    interest = models.CharField(max_length=500, null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeJobPreferences(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_preference"
    )
    industory = models.ForeignKey(
        Industory,
        on_delete=models.CASCADE,
        null=True,
        related_name="employee_preference_industory",
    )
    job_time_type = models.CharField(max_length=500, null=True, blank=True, default=" ")
    job_type = models.CharField(max_length=500, null=True, blank=True, default=" ")
    location = models.CharField(max_length=500, null=True, blank=True, default=" ")
    job_functinal = models.CharField(max_length=500, null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeProjects(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_project"
    )
    title = models.CharField(max_length=500, null=True, blank=True)
    affiliated_company = models.CharField(
        max_length=500, null=True, blank=True, default=" "
    )
    start_date = models.DateField(null=True, blank=True, default=" ")
    end_date = models.DateField(null=True, blank=True, default=" ")
    description = models.TextField(null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeSkills(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_skills"
    )
    skill = models.ForeignKey(
        Skill, on_delete=models.CASCADE, null=True, related_name="employee_skill"
    )
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeCertification(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_certificate"
    )
    course = models.CharField(max_length=500, null=True, blank=True, default=" ")
    issued_by = models.CharField(max_length=500, null=True, blank=True, default=" ")
    date = models.DateField(null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeReference(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_reference"
    )
    name = models.CharField(max_length=500, null=True, blank=True, default=" ")
    mobile = models.CharField(max_length=500, null=True, blank=True, default=" ")
    email = models.EmailField(max_length=500, null=True, blank=True, default=" ")
    company = models.CharField(max_length=500, null=True, blank=True, default=" ")
    position = models.CharField(max_length=500, null=True, blank=True, default=" ")
    relationship_type = models.CharField(
        max_length=500, null=True, blank=True, default=" "
    )
    description = models.TextField(null=True, blank=True, default=" ")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeInterviewSchedule(models.Model):
    PENDING = 0
    SELECTED = 1
    REJECTED = 2
    HOLD = 3

    Status_choices = [
        (PENDING, "Pending"),
        (SELECTED, "Selected"),
        (REJECTED, "Rejected"),
        (HOLD, "Hold"),
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job_id = models.ForeignKey(Job, on_delete=models.CASCADE)
    user_id = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        related_name="employee_interview_schedule",
    )
    hr_id = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="hr_user"
    )
    time_slot = models.ForeignKey(
        InterviewTimeSlot,
        on_delete=models.SET_NULL,
        null=True,
        related_name="interview_time_slot",
    )
    interview_date = models.DateField(null=True)
    status = models.SmallIntegerField(default=0, choices=Status_choices)
    is_selected = models.BooleanField(default=False)
    is_call_done = models.BooleanField(default=False)
    room_id = models.CharField(null=True, max_length=500, blank=True)
    agora_token_hr = models.CharField(null=True, max_length=500, blank=True)
    agora_token_employee = models.CharField(null=True, max_length=500, blank=True)
    agora_uid_hr = models.CharField(null=True, max_length=500, blank=True)
    agora_uid_employee = models.CharField(null=True, max_length=500, blank=True)
    agora_uid = models.CharField(null=True, max_length=500, blank=True)
    agora_resource_id = models.CharField(null=True, max_length=500, blank=True)
    agora_start_id = models.CharField(null=True, max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class InterViewRecording(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    interview_id = models.ForeignKey(
        EmployeeInterviewSchedule,
        on_delete=models.CASCADE,
        related_name="interview_recording",
    )
    video_url = models.CharField(null=True, max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeInterviewRemark(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    interview_id = models.ForeignKey(
        EmployeeInterviewSchedule,
        on_delete=models.CASCADE,
        related_name="interview_remark",
    )
    skill_id = models.ForeignKey(InterviewSkills, on_delete=models.CASCADE)
    marks = models.IntegerField(default=0)
    details = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class EmployeeContactList(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="employee_contact_list"
    )
    name = models.CharField(max_length=500, null=True, blank=True)
    contacts = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
