from oauth2_provider.contrib.rest_framework import TokenHasScope, OAuth2Authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from .serializers import *
from rest_framework import generics, permissions, serializers
from rest_framework.generics import *
from rest_framework.mixins import *
from jobspri.agoro.RtcTokenBuilderTest import getRTCAgoraToken
import random
from accounts.models import *
from jobspri_admin.models import Skill
from company.models import *
from company.serializers import *
from .models import *

class EmployeeProfileView(
    generics.GenericAPIView,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ProfileSerializer
    queryset = EmployeeProfile.objects.all()
    lookup_field = "user_id"

    def get(self, request, user_id=None):
        return self.retrieve(request, self.request.user)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Profile created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def get_object(self, queryset=None):
        obj = EmployeeProfile.objects.get(user_id=self.request.user.id)
        return obj

    def put(self, request, user_id=None):
        self.update(request, self.request.user)
        return Response(
            {
                "message": "Profile updated successfully",
            }
        )


class PriviousWorkExperienceView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PriviousWorkExperienceSerializer
    queryset = EmployeePriviousWorkExperience.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Work experience created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Work experience updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Work experience deleted successfully"})


class EmployeeEducationView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeEducationSerializer
    queryset = EmployeeEducation.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Education created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Education updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Education deleted successfully"})


class EmployeeProjectsView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeProjectsSerializer
    queryset = EmployeeProjects.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Project created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Project updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Project deleted successfully"})


class EmployeeJobPreferencesView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeJobPreferencesSerializer
    queryset = EmployeeJobPreferences.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Job preference created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Job preference updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Job preference deleted successfully"})


class EmployeeSkillsView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeSkillsSerializer
    queryset = EmployeeSkills.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Skill created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Skill updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Skill deleted successfully"})


class EmployeeBulkSkillsView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    queryset = EmployeeSkills.objects.all()
    lookup_field = "id"

    def post(self, request):
        skills = request.data["skills"]
        EmployeeSkills.objects.filter(user_id=self.request.user).delete()
        for i in skills:
            EmployeeSkills.objects.create(
                skill=Skill(i["skill"]), user_id=self.request.user
            )
        return Response(
            {
                "message": "Skills added successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)


class EmployeeInterestView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeInterestSerializer
    queryset = EmployeeInterest.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Interest created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Interest updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Interest deleted successfully"})


class EmployeeAchievementsView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeAchievementsSerializer
    queryset = EmployeeAchievements.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Achievement created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Achievement updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Achievement deleted successfully"})


class EmployeeCertificationView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):

    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeCertificationSerializer
    queryset = EmployeeCertification.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Certification created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request)
        return Response(
            {
                "message": "Certification updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Certification deleted successfully"})


class EmployeeReferenceView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = EmployeeReferenceSerializer
    queryset = EmployeeReference.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        self.create(request)
        return Response(
            {
                "message": "Reference created successfully",
            }
        )

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)

    def put(self, request, id=None):
        self.update(request, self.request.user)
        return Response(
            {
                "message": "Reference updated successfully",
            }
        )

    def delete(self, request, id=None):
        self.destroy(request, id)
        return Response({"message": "Reference deleted successfully"})


class EmployeeProfilePictureView(generics.GenericAPIView, mixins.UpdateModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UpdateProfilePictureSerializer
    model = User
    lookup_field = "user_id"

    def get_object(self, queryset=None):
        return User.objects.get(id=self.request.user.id)

    def get(self, request, user_id=None):
        try:
            obj = User.objects.get(id=self.request.user.id)
            url = obj.profile_picture.url
        except:
            url = ""
        return Response({"profile_picture": url})

    def put(self, request, user_id=None):
        response = self.update(request, self.request.user)
        return Response(
            {
                "message": "Profile updated successfully",
            }
        )


class InterviewScheduleForDateView(GenericAPIView, ListModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = InterviewTimeSlotSerializer
    model = InterviewTimeSlot
    lookup_field = "id"
    queryset = InterviewTimeSlot.objects.all()

    def get_queryset(self):
        # slots = []
        # hrs = User.objects.filter(role=User.HR)
        # for slot in hrs:
        #     slots.append(InterviewTimeSlot.objects.all())
        # # print(slots)
        # combinations = []
        # for levels in itertools.product(*levels):
        #     combination = {}
        #     combination["levels"] = levels
        #     combinations.append(combination)

        date = self.request.query_params.get("date")
        date = datetime.strptime(date, "%Y-%m-%d").date()
        today = datetime.now().date()
        if date == today:
            now = datetime.now().strftime("%H:%M:%S")
            return self.queryset.filter(time__gte=now)
        else:
            return self.queryset

    def get(self, request, id=None):
        return self.list(request)


class EmployeeInterviewScheduleView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ScheduleInterviewSerializer
    queryset = EmployeeInterviewSchedule.objects.all()
    lookup_field = "id"

    def get_queryset(self):

        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            obj = EmployeeInterviewSchedule.objects.get(id=id)
            obj.agora_token_employee = getRTCAgoraToken(
                obj.room_id, obj.agora_uid_employee
            )
            obj.save()

            return self.retrieve(request, id)
        else:
            return self.list(request)


class EmployeeInterviewScheduleCreateView(
    generics.GenericAPIView,
    mixins.CreateModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = ScheduleInterviewCreateSerializer
    queryset = EmployeeInterviewSchedule.objects.all()
    lookup_field = "id"

    def post(self, request):
        self.create(request)
        data = Job.objects.filter(employeeinterviewschedule__user_id=self.request.user)
        serializer = JobSerializer(data, many=True)

        return Response({"jobs": serializer.data})

    def perform_create(self, serializer):
        room_id = str(random.randint(100000, 999999))
        agora_uid_hr = str(random.randint(1000000, 9999999))
        agora_uid_employee = str(random.randint(1000000, 9999999))

        agora_token_hr = getRTCAgoraToken(room_id, agora_uid_hr)
        agora_token_employee = getRTCAgoraToken(room_id, agora_uid_employee)

        serializer.save(
            user_id=self.request.user,
            room_id=room_id,
            agora_token_hr=agora_token_hr,
            agora_token_employee=agora_token_employee,
            agora_uid_hr=agora_uid_hr,
            agora_uid_employee=agora_uid_employee,
        )


class UserNotificationView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserNotificationSerializer
    queryset = UserNotification.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id)

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class EmployeeContactListView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        for i in data["contacts"]:
            test = EmployeeContactList.objects.create(
                user_id=request.user, name=i["name"], contacts=i["numbers"]
            )
        return Response({"Message": "Contact added successfully"})


class EmployeeContactListCheckView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id=None):
        is_exists = EmployeeContactList.objects.filter(user_id=request.user).exists()
        return Response({"is_contact_added": is_exists})


