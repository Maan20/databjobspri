from django.contrib import admin
from employee_profile.models import *
from accounts.models import *
import nested_admin

class EmployeeProfileAdmin(admin.ModelAdmin):
    search_fields = ["id", "user_id"]
    list_display = ("id", "user_id")


class EmployeeSkillsAdmin(admin.ModelAdmin):
    search_fields = ["id", "user_id"]
    list_display = ("id", "user_id")


class EmployeePriviousWorkExperienceAdmin(admin.ModelAdmin):
    search_fields = ["id", "user_id"]
    list_display = ("id", "user_id")


class EmployeeEducationAdmin(admin.ModelAdmin):
    search_fields = ["id", "user_id"]
    list_display = ("id", "user_id")


class InterViewRecordingAdmin(nested_admin.NestedTabularInline):
    model = InterViewRecording
    extra = 0


class EmployeeInterviewRemarkAdmin(nested_admin.NestedTabularInline):
    model = EmployeeInterviewRemark
    extra = 0


class EmployeeInterviewScheduleAdmin(nested_admin.NestedModelAdmin):
    list_display = ["job_id", "user_id", "hr_id", "status", "interview_date", "time_slot"]
    model = EmployeeInterviewSchedule
    inlines = [InterViewRecordingAdmin, EmployeeInterviewRemarkAdmin]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "hr_id":
            kwargs["queryset"] = User.objects.filter(role=User.HR)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class EmployeeContactListAdmin(admin.ModelAdmin):
    search_fields = ["id", "user_id"]
    list_display = ("id", "user_id")


admin.site.register(EmployeeInterviewSchedule, EmployeeInterviewScheduleAdmin)
admin.site.register(EmployeeProfile, EmployeeProfileAdmin)
admin.site.register(EmployeePriviousWorkExperience, EmployeePriviousWorkExperienceAdmin)
admin.site.register(EmployeeEducation, EmployeeEducationAdmin)
admin.site.register(EmployeeContactList, EmployeeContactListAdmin)
admin.site.register(EmployeeSkills, EmployeeSkillsAdmin)
admin.site.register(EmployeeProjects)
admin.site.register(EmployeeJobPreferences)
admin.site.register(EmployeeAchievements)
admin.site.register(EmployeeCertification)
admin.site.register(EmployeeReference)
