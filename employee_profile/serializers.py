from rest_framework import serializers, exceptions
from accounts.models import *
from .models import *
from jobspri_admin.models import *
from jobspri_admin.serializers import *
from company.serializers import JobSerializer
from django.core.files.base import ContentFile
import base64
from PIL import Image
import io
from datetime import datetime
from rest_framework import exceptions
from django.utils.translation import gettext_lazy as _


class PriviousWorkExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeePriviousWorkExperience
        fields = "__all__"


class EmployeeEducationSerializer(serializers.ModelSerializer):

    course_name = serializers.CharField(read_only=True, source="course.name")
    specialization_name = serializers.CharField(
        read_only=True, source="specialization.name"
    )

    class Meta:
        model = EmployeeEducation
        fields = "__all__"


class EmployeeSkillsSerializer(serializers.ModelSerializer):
    skills_details = SkillsSerializer(read_only=True, source="skill")
    skills_name = serializers.CharField(read_only=True, source="skill.name")

    class Meta:
        model = EmployeeSkills
        fields = "__all__"


class EmployeeInterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeInterest
        fields = "__all__"


class EmployeeProjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProjects
        fields = "__all__"


class EmployeeJobPreferencesSerializer(serializers.ModelSerializer):
    industory_name = serializers.CharField(read_only=True, source="industory.name")

    class Meta:
        model = EmployeeJobPreferences
        fields = "__all__"


class EmployeeAchievementsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeAchievements
        fields = "__all__"


class EmployeeCertificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeCertification
        fields = "__all__"


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProfile
        exclude = ("profile_picture",)


class EmployeeReferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeReference
        fields = "__all__"


class InterViewRecordingSerializer(serializers.ModelSerializer):
    class Meta:
        model = InterViewRecording
        fields = "__all__"


class UpdateProfilePictureSerializer(serializers.Serializer):
    profile_picture = serializers.CharField(allow_null=True)

    def update(self, instance, validated_data):
        # if validated_data["profile_picture"] != None and validated_data["profile_picture"].startswith("data"):
        image_data = validated_data["profile_picture"]
        format, imgstr = image_data.split(";base64,")
        file_name = str(datetime.now().timestamp())
        data = ContentFile(base64.b64decode(imgstr), name=file_name + "." + "jpeg")
        instance.profile_picture = data
        instance.save()
        return instance


class InterviewTimeSlotSerializer(serializers.ModelSerializer):
    class Meta:
        model = InterviewTimeSlot
        fields = "__all__"


class EmployeeContactListSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeContactList
        fields = "__all__"


class ScheduleInterviewSerializer(serializers.ModelSerializer):
    time_slot = InterviewTimeSlotSerializer()
    job_id = JobSerializer()
    is_ready_for_interview = serializers.SerializerMethodField(
        "check_if_ready_for_interview", read_only=True
    )
    status = serializers.CharField(source="get_status_display")

    def check_if_ready_for_interview(self, obj):
        interview_date = obj.interview_date
        interview_time = obj.time_slot.time
        interview_date_time = datetime.combine(interview_date, interview_time)
        today = datetime.now()
        total_seconds = (interview_date_time - today).total_seconds()
        diff = total_seconds / 60

        if diff <= 10:
            if obj.is_call_done == True:
                return False
            return True

        else:
            return False

    class Meta:
        model = EmployeeInterviewSchedule
        fields = "__all__"


class ScheduleInterviewCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeInterviewSchedule
        fields = "__all__"

    def validate(self, data):
        complate = True
        complate = EmployeePriviousWorkExperience.objects.filter(
            user_id=self.context["request"].user
        ).exists()
        if complate:
            complate = EmployeeEducation.objects.filter(
                user_id=self.context["request"].user
            ).exists()
        if complate:
            complate = EmployeeInterest.objects.filter(
                user_id=self.context["request"].user
            ).exists()
        if complate:
            complate = EmployeeJobPreferences.objects.filter(
                user_id=self.context["request"].user
            ).exists()
        if complate:
            complate = EmployeeProjects.objects.filter(
                user_id=self.context["request"].user
            ).exists()
        if complate:
            complate = EmployeeSkills.objects.filter(
                user_id=self.context["request"].user
            ).exists()
        if complate:
            return data
        else:
            return data
            msg = "Please complete your profile (Work experience, Education ,Interest, Job Preferences, Projects, Skills) before apply for job"

            raise exceptions.ValidationError({"Message": msg})


class UserNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserNotification
        fields = ("id", "notification_id", "is_read")
        depth = 2


class ProfileDetailsSerializer(serializers.ModelSerializer):
    employee_profile = ProfileSerializer()
    employee_work_experience = PriviousWorkExperienceSerializer(
        required=False, many=True
    )
    employee_eductaion = EmployeeEducationSerializer(required=False, many=True)
    employee_achievement = EmployeeAchievementsSerializer(required=False, many=True)
    employee_preference = EmployeeJobPreferencesSerializer(required=False, many=True)
    employee_project = EmployeeProjectsSerializer(required=False, many=True)
    employee_skills = EmployeeSkillsSerializer(many=True, required=False)
    employee_certificate = EmployeeCertificationSerializer(many=True, required=False)
    profile_picture_url = serializers.SerializerMethodField(
        "get_profile_picture_url", read_only=True
    )

    def get_profile_picture_url(self, obj):
        try:
            return obj.profile_picture.url
        except:
            return " "

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "mobile",
            "profile_picture_url",
            "employee_certificate",
            "employee_skills",
            "employee_project",
            "employee_preference",
            "employee_achievement",
            "employee_eductaion",
            "employee_work_experience",
            "employee_profile",
            "created_at",
        )
