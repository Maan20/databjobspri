# Generated by Django 3.1 on 2022-07-03 19:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('company', '0001_initial'),
        ('jobspri_admin', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='EmployeeInterviewSchedule',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('interview_date', models.DateField(null=True)),
                ('status', models.SmallIntegerField(choices=[(0, 'Pending'), (1, 'Selected'), (2, 'Rejected'), (3, 'Hold')], default=0)),
                ('is_selected', models.BooleanField(default=False)),
                ('is_call_done', models.BooleanField(default=False)),
                ('room_id', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_token_hr', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_token_employee', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_uid_hr', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_uid_employee', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_uid', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_resource_id', models.CharField(blank=True, max_length=500, null=True)),
                ('agora_start_id', models.CharField(blank=True, max_length=500, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('hr_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='hr_user', to=settings.AUTH_USER_MODEL)),
                ('job_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.job')),
                ('time_slot', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='interview_time_slot', to='jobspri_admin.interviewtimeslot')),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_interview_schedule', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='InterViewRecording',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('video_url', models.CharField(blank=True, max_length=500, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('interview_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='interview_recording', to='employee_profile.employeeinterviewschedule')),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeSkills',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('skill', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_skill', to='jobspri_admin.skill')),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_skills', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeReference',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('mobile', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('email', models.EmailField(blank=True, default=' ', max_length=500, null=True)),
                ('company', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('position', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('relationship_type', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('description', models.TextField(blank=True, default=' ', null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_reference', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeProjects',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, max_length=500, null=True)),
                ('affiliated_company', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('start_date', models.DateField(blank=True, default=' ', null=True)),
                ('end_date', models.DateField(blank=True, default=' ', null=True)),
                ('description', models.TextField(blank=True, default=' ', null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_project', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeProfile',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('gender', models.CharField(blank=True, default=' ', max_length=10, null=True)),
                ('profile_picture', models.ImageField(blank=True, null=True, upload_to='profile_pictures/%Y/%m/%d/')),
                ('summary', models.TextField(blank=True, default=' ', null=True)),
                ('linkedin_profile_url', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('total_work_experience', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('current_location', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeePriviousWorkExperience',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('company_name', models.CharField(blank=True, max_length=500, null=True)),
                ('designation', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('is_current_company', models.BooleanField(default=False)),
                ('started_working_wrom', models.DateField(blank=True, default=' ', null=True)),
                ('description', models.TextField(blank=True, default=' ', null=True)),
                ('worked_till', models.DateField(blank=True, default=' ', null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_work_experience', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeJobPreferences',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('job_time_type', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('job_type', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('location', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('job_functinal', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('industory', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_preference_industory', to='jobspri_admin.industory')),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_preference', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeInterviewRemark',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('marks', models.IntegerField(default=0)),
                ('details', models.TextField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('interview_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='interview_remark', to='employee_profile.employeeinterviewschedule')),
                ('skill_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jobspri_admin.interviewskills')),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeInterest',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('interest', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_interest', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeEducation',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('university', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('type', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('start_date', models.DateField(blank=True, default=' ', null=True)),
                ('end_date', models.DateField(blank=True, default=' ', null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('course', models.ForeignKey(default=' ', null=True, on_delete=django.db.models.deletion.SET_NULL, to='jobspri_admin.course')),
                ('specialization', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='jobspri_admin.specialization')),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_eductaion', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeContactList',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=500, null=True)),
                ('contacts', models.JSONField()),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_contact_list', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeCertification',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('course', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('issued_by', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('date', models.DateField(blank=True, default=' ', null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_certificate', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EmployeeAchievements',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(blank=True, default=' ', max_length=500, null=True)),
                ('description', models.TextField(blank=True, default=' ', null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('user_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='employee_achievement', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
