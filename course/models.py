from django.db import models
import uuid
from accounts.models import *
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=500, null=False, blank=False)
    details = models.TextField(null=True, blank=False)
    thumbnail = models.ImageField(
        null=True, max_length=255, upload_to="course/%Y/%m/%d/", blank=True
    )
    parent_category = models.ForeignKey(
        "self",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="category_parent",
    )
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class Courses(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=500, null=False, blank=False)
    details = models.TextField(null=True, blank=False)
    author = models.CharField(max_length=500, null=False, blank=False)
    thumbnail = models.ImageField(
        null=True, max_length=255, upload_to="course/%Y/%m/%d/", blank=True
    )
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="course_category"
    )
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class CourseAssessmentModule(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    course_id = models.ForeignKey(
        Courses, on_delete=models.CASCADE, related_name="course_assessment"
    )
    module_name = models.CharField(max_length=500, null=False, blank=False)
    video = models.FileField(null=True, blank=True, upload_to="course_video/%Y/%m/%d/")
    video_id = models.TextField(max_length=1000, null=True, blank=True)
    min_marks = models.IntegerField(
        help_text=_("Enter Minimum marks in percentage"),
    )
    no_of_questions_in_test = models.IntegerField(default=1)
    order = models.IntegerField(default=1)
    duration = models.CharField(
        null=True,
        max_length=100,
        blank=True,
        default=30,
        help_text=_("Enter duration in minutes example 60"),
    )
    details = models.TextField(null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.module_name


class CourseAssessmentQuestion(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    course_assessment_id = models.ForeignKey(
        CourseAssessmentModule, on_delete=models.CASCADE, related_name="course_question"
    )
    question = models.TextField()
    remark = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.question


class CourseAssessmentQuestionsAnswer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    question = models.ForeignKey(
        CourseAssessmentQuestion,
        on_delete=models.CASCADE,
        related_name="course_questions_options",
    )
    option = models.CharField(max_length=1000)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.option


class CourseApplication(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    course_id = models.ForeignKey(
        Courses, on_delete=models.CASCADE, related_name="course_application"
    )
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="course_application"
    )
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class AssessmentTestTakers(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_test")
    course_id = models.ForeignKey(Courses, on_delete=models.CASCADE)
    assessment_id = models.ForeignKey(CourseAssessmentModule, on_delete=models.CASCADE)
    correct_answers = models.IntegerField(default=0)
    total_question = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    is_assessment_passed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class AssessmentTestResponse(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    quiztaker = models.ForeignKey(
        AssessmentTestTakers, on_delete=models.CASCADE, related_name="question_set"
    )
    question = models.ForeignKey(
        CourseAssessmentQuestion, on_delete=models.CASCADE, related_name="question_set"
    )
    answer = models.ForeignKey(
        CourseAssessmentQuestionsAnswer, on_delete=models.CASCADE, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class AssessmentCertificate(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    course_id = models.ForeignKey(Courses, on_delete=models.CASCADE)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="course_certificate"
    )
    certificate_url = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
