from rest_framework import serializers
from .models import *
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from rest_framework import exceptions
from employee_profile.models import *


class CourseSerializer(serializers.ModelSerializer):

    is_applied = serializers.SerializerMethodField("check_if_applied", read_only=True)
    all_modules_clear = serializers.SerializerMethodField(
        "check_all_modules_clear", read_only=True
    )
    modules_clear_percent = serializers.SerializerMethodField(
        "check_modules_clear_percent", read_only=True
    )

    def check_all_modules_clear(self, obj):
        assessments_count = CourseAssessmentModule.objects.filter(course_id=obj.id).count()
        total_passed = AssessmentTestTakers.objects.filter(
            assessment_id=obj.id,
            user=self.context["request"].user,
            is_assessment_passed=True,
        ).count()
        if assessments_count == total_passed:
            return True
        else:
            return False

    def check_modules_clear_percent(self, obj):
        percent = 0
        count = 0
        try:
            assessments = CourseAssessmentModule.objects.filter(course_id=obj.id)
            for ass in assessments:
                is_exists = AssessmentTestTakers.objects.filter(
                    assessment_id=obj.id,
                    user=self.context["request"].user,
                    is_assessment_passed=True,
                ).exists()
            count = count + 1
            total = assessments.count()
            percent = int((count / total) * 100)
            return percent

        except:
            return 0

  

    def check_if_applied(self, obj):
        try:
            is_applied = CourseApplication.objects.filter(
                course_id=obj.id, user_id=self.context["request"].user
            ).exists()
            return is_applied
        except:
            return False


    class Meta:
        model = Courses
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    course_category = CourseSerializer(many=True)
    class Meta:
        model = Category
        fields = "__all__"

class CourseAssessmentVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseAssessmentModule
        fields = ("video","video_id","details")


class CourseAssessmentModuleListSerializer(serializers.ModelSerializer):
    is_viewed = serializers.SerializerMethodField("check_is_viewed", read_only=True)
    is_assessment_passed = serializers.SerializerMethodField(
        "check_is_assessment_passed", read_only=True
    )

    def check_is_viewed(self, obj):
        try:
            is_applied = AssessmentTestTakers.objects.filter(
                assessment_id=obj.id, user=self.context["request"].user
            ).exists()
            return is_applied
        except:
            return False

    def check_is_assessment_passed(self, obj):
        try:
            is_applied = AssessmentTestTakers.objects.filter(
                assessment_id=obj.id,
                user=self.context["request"].user,
                is_assessment_passed=True,
            ).exists()
            return is_applied
        except:
            return False

    class Meta:
        model = CourseAssessmentModule
        fields = ("id", "module_name","details", "is_viewed", "is_assessment_passed", "order","video_id","video")


class CourseAssessmentQuestionsAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseAssessmentQuestionsAnswer
        exclude = ("question",)


class CourseAssessmentQuestionSerializer(serializers.ModelSerializer):
    course_questions_options = CourseAssessmentQuestionsAnswerSerializer(many=True)

    class Meta:
        model = CourseAssessmentQuestion
        fields = "__all__"


class CourseAssessmentDetailsSerializer(serializers.ModelSerializer):
    course_question = CourseAssessmentQuestionSerializer(many=True)

    class Meta:
        model = CourseAssessmentModule
        fields = "__all__"


class CourseApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseApplication
        fields = ("course_id",)


class CourseAssessmentTestSerializer(serializers.Serializer):
    question_data = serializers.JSONField()
    course_id = serializers.UUIDField(format="hex_verbose")
    assessment_id = serializers.UUIDField(format="hex_verbose")

    def validate(self, data):
        request = self.context.get("request")
        print(request)
        return data


class AssessmentTestResponseSerializer(serializers.ModelSerializer):
    options = serializers.SerializerMethodField("get_guestions_options", read_only=True)

    class Meta:
        model = AssessmentTestResponse
        exclude = ("quiztaker", "id")
        depth = 1

    def get_guestions_options(self, obj):
        options = CourseAssessmentQuestionsAnswer.objects.filter(question=obj.question)
        return CourseAssessmentQuestionsAnswerSerializer(options, many=True).data


class AssessmentTestTakersSerializer(serializers.ModelSerializer):
    question_set = AssessmentTestResponseSerializer(many=True)

    class Meta:
        model = AssessmentTestTakers
        fields = "__all__"