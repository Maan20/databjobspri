from django.conf.urls import url
from django.urls import path
from course.views import *

urlpatterns = [
    path("category/", CategoryView.as_view()),
    path("category/<id>", CategoryView.as_view()),
    path("course-list", CourseView.as_view()),
    path("course-details/<id>", CourseView.as_view()),
    path("course/<course_id>/assessment-modules",CourseAssessmentModuleView.as_view()),
    path("course/<pk>/assessment-video", CourseAssessmentVideoView.as_view()),
    path("course/<id>/assessment-details",CourseAssessmentDetailsView.as_view()),
    path("course/<id>/certificate",CourseCertificateView.as_view()),
    path("course/application/", CourseApplicationView.as_view()),
    path("course/assessment-test/", CourseAssessmentTestView.as_view()),
    path("course/assessment-test/<id>/report", CourseAssessmentTestResult.as_view()),
]