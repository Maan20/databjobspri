from django.contrib import admin
from course.models import *
import nested_admin



class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['id','name']
    list_display = ('id','name')

class CourseAssessmentQuestionsAnswerInline(nested_admin.NestedTabularInline):
    model = CourseAssessmentQuestionsAnswer
    max_num = 4
    extra =  4


class CourseAssessmentQuestionInline(nested_admin.NestedTabularInline):
    model = CourseAssessmentQuestion
    inlines = [CourseAssessmentQuestionsAnswerInline,]
    extra = 0

class CourseAssessmentInline(nested_admin.NestedModelAdmin):
    model = CourseAssessmentModule
    list_display = ('id','course_id','module_name')
    inlines = [CourseAssessmentQuestionInline,]


  
class CourseAdmin(nested_admin.NestedModelAdmin):
    model = Courses
    search_fields = ['id','name','author']
    list_display = ('name','author')
   

class AssessmentTestResponseInline(nested_admin.NestedTabularInline):
    model = AssessmentTestResponse

class AssessmentTestTakersAdmin(nested_admin.NestedModelAdmin):
    list_display = ('course_id','user','assessment_id')
    inlines = [AssessmentTestResponseInline,]
    model = AssessmentTestTakers

class CourseApplicationAdmin(admin.ModelAdmin):
    list_display = ('course_id','user_id')


admin.site.register(Category,CategoryAdmin)
admin.site.register(CourseApplication,CourseApplicationAdmin)

admin.site.register(Courses,CourseAdmin)
admin.site.register(CourseAssessmentModule,CourseAssessmentInline)
admin.site.register(AssessmentTestTakers,AssessmentTestTakersAdmin)


