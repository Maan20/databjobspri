from django.http import request
from oauth2_provider.contrib.rest_framework import TokenHasScope, OAuth2Authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions, generics, serializers
from .serializers import *
from rest_framework.generics import *
from rest_framework.mixins import *
from .models import *
from datetime import datetime, timedelta
from django.db.models import Q


class CategoryView(GenericAPIView, ListModelMixin, RetrieveModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class CourseView(
    GenericAPIView,
    ListModelMixin,
    CreateModelMixin,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseSerializer
    queryset = Courses.objects.all()
    lookup_field = "id"

    def get_queryset(self):

        queryset = self.queryset
        if self.request.query_params.get("search"):
            q = self.request.query_params.get("search")
            queryset = queryset.filter(name__icontains=q).distinct()
        if self.request.query_params.get("category"):
            category = self.request.query_params.get("category")
            queryset = queryset.filter(category__name=category)
        if self.request.query_params.get("all_clear"):
            to_be_remove = []
            queryset = Courses.objects.all()
            for dd in queryset:
                assessments_count = CourseAssessmentModule.objects.filter(
                    course_id=dd.id
                ).count()
                total_passed = AssessmentTestTakers.objects.filter(
                    assessment_id=dd.id,
                    user=self.request.user,
                    is_assessment_passed=True,
                ).count()
                if assessments_count != total_passed:
                    to_be_remove.append(dd.id)
                queryset = queryset.exclude(id__in=to_be_remove)
        if self.request.query_params.get("applied"):
            queryset = queryset.filter(course_application__user_id=self.request.user.id)
        return queryset

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)


class CourseAssessmentModuleView(
    GenericAPIView,
    ListModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseAssessmentModuleListSerializer
    queryset = CourseAssessmentModule.objects.all()
    lookup_field = "course_id"

    def get_queryset(self):
        queryset = self.queryset
        course_id = self.kwargs["course_id"]
        queryset = queryset.filter(course_id=course_id).order_by("order")
        return queryset

    def get(self, request, course_id=None):
        return self.list(request)


class CourseAssessmentDetailsView(
    GenericAPIView,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseAssessmentDetailsSerializer
    queryset = CourseAssessmentModule.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        return self.retrieve(request, id)


class CourseApplicationView(
    GenericAPIView,
    CreateModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseApplicationSerializer

    def post(self, request):
        self.create(request)
        data = Course.objects.filter(
            course_application__user_id=self.request.user
        ).exclude(employeeinterviewschedule__user_id=self.request.user)
        serializer = CourseSerializer(data, many=True)
        return Response({"applied_courses": serializer.data})

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user)


class CourseAssessmentTestView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseApplicationSerializer

    def post(self, request):
        serializer = CourseAssessmentTestSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        data = request.data

        correct_answers = 0
        percentage = 0
        result = False
        test = AssessmentTestTakers.objects.create(
            course_id=Courses(data["course_id"]),
            assessment_id=CourseAssessmentModule(data["assessment_id"]),
            user=request.user,
        )

        for i in data["question_data"]:
            is_correct = CourseAssessmentQuestionsAnswer.objects.filter(
                id=i["answer"], is_correct=True
            ).count()
            correct_answers += is_correct
            AssessmentTestResponse.objects.create(
                quiztaker=test,
                question=CourseAssessmentQuestion(i["questions"]),
                answer=CourseAssessmentQuestionsAnswer(i["answer"]),
            )

        assessment = CourseAssessmentModule.objects.get(id=data["assessment_id"])
        total_question = assessment.course_question.count()

        min_marks = int(assessment.min_marks)

        if correct_answers > 0:
            percentage = int((correct_answers * 100) / total_question)

        if percentage >= min_marks:
            result = True

        test.correct_answers = correct_answers
        test.completed = True
        test.is_assessment_passed = result
        test.total_question = total_question
        test.save()

        return Response(
            {
                "id": test.id,
                "message": "Test submitted successfully",
                "correct_answers": correct_answers,
                "assessment_passed": result,
                "total_question": total_question,
                "wrong_answers": total_question - correct_answers,
                "percentage": percentage,
                "min_marks": min_marks,
            }
        )


class CourseAssessmentTestResult(
    GenericAPIView,
    RetrieveModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = AssessmentTestTakersSerializer
    queryset = AssessmentTestTakers.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        return self.retrieve(request, id)


class CourseAssessmentVideoView(
    generics.GenericAPIView, mixins.RetrieveModelMixin, mixins.ListModelMixin
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseAssessmentVideoSerializer

    def get_queryset(self):
        return CourseAssessmentModule.objects.all()

    def get(self, request, pk=None):
        return self.retrieve(request, pk)


class CourseCertificateView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CourseSerializer
    queryset = Courses.objects.all()
    lookup_field = "id"

    def get(self, request, id=None):
        assessments_count = CourseAssessmentModule.objects.filter(course_id=id).count()
        total_passed = AssessmentTestTakers.objects.filter(
            assessment_id=id, user=request.user, is_assessment_passed=True
        ).count()

        if assessments_count == total_passed and total_passed != 0:
            return Response({"certificate_url": "http://www.africau.edu/images/default/sample.pdf"})
        else:
            return Response({"message": "Invalid "}, 400)
