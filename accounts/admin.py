from django.contrib import admin
from accounts.models import *


class UserAdmin(admin.ModelAdmin):
    list_display = [
        "email",
        "mobile",
        "first_name",
        "last_name",
        "is_active",
        "role",
        "created_at",
        "updated_at",
    ]


class HrContactAdmin(admin.ModelAdmin):
    list_display = ["id", "hr_id"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "hr_id":
            kwargs["queryset"] = User.objects.filter(role=User.HR)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "cadidate_id":
            kwargs["queryset"] = User.objects.filter(role=User.EMPLOYEE)
        return super().formfield_for_manytomany(db_field, request, **kwargs)


admin.site.register(User, UserAdmin)
admin.site.register(HrContact, HrContactAdmin)
admin.site.register(UserNotification)
admin.site.register(Notification)
