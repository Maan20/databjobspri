from django.conf.urls import url
from django.urls import path
from oauth2_provider import views as oauth2_views
from accounts.views import *

urlpatterns = [
    path("user/register/", UserRegister.as_view()),
    path("login/", LoginView.as_view()),
    path("user/<id>/", UserView.as_view()),
    path("send-otp/", SendOTPView.as_view()),
    path("send-otp/<type>/", SendOTPView.as_view()),
    path("verify-otp/", OTPVerificationView.as_view()),
    path("password/verify-otp/", OTPVerificationForChangePasswordView.as_view()),
    path("password/update/", UpdatePasswordView.as_view()),
    path(
        "device-token/",
        FCMTokenUpdate.as_view(),
    ),
]