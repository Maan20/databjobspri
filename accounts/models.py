from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from datetime import datetime
import uuid


class UserManager(BaseUserManager):
    """
    A custom user manager to deal with mobiles as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """

    def _create_user(self, mobile, password, **extra_fields):
        """
        Creates and saves a User with the given mobile and password.
        """
        if not mobile:
            raise ValueError("The mobile must be set")
        # username = self.normalize_username(username)
        user = self.model(mobile=mobile, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, mobile, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")
        return self._create_user(mobile, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    ADMIN = 0
    EMPLOYEE = 1
    HR = 2
    USER_ROLE = [(ADMIN, "admin"), (EMPLOYEE, "Candidate"), (HR, "HR")]


    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False,unique=True)
    email = models.EmailField(null=True, blank=False)
    first_name = models.CharField(max_length=50, null=True, blank=False)
    last_name = models.CharField(max_length=50, null=True, blank=True,default="")
    mobile = models.CharField(max_length=30, null=True, blank=False,unique=True)
    profile_picture = models.ImageField(
        null=True, max_length=255, upload_to="profile_pictures/%Y/%m/%d/", blank=True
    )
    role = models.IntegerField(choices=USER_ROLE, null=True, blank=False)
    is_first_login = models.IntegerField(null=True, default="1")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    is_staff = models.BooleanField(
        _("staff"),
        default=False,
        help_text=_("Designates whether this user should be treated as staff. "),
    )
    otp = models.CharField(max_length=10, null=True, blank=True)
    is_active = models.BooleanField(
        _("active"),
        default=False,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    password_token = models.UUIDField(
        default=uuid.uuid4, editable=True, null=True, blank=True
    )
    USERNAME_FIELD = "mobile"
    objects = UserManager()

    class Meta:
        db_table = "user"
        ordering = ['-created_at']

    def __str__(self):
        # return str(self.first_name) + " " + str(self.last_name)
        return str(self.first_name)


    def fullname(self):
        # return str(self.first_name) + " " + str(self.last_name)
        return str(self.first_name)


class Notification(models.Model):
    JOB = 0
    PROFILE = 1
    INTERVIEW = 2
    TEST = 3
    TYPE = [(JOB, "job"), (PROFILE, "profile"), (INTERVIEW, "interview"), (TEST, "test")]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False,unique=True)
    type = models.IntegerField(choices=TYPE, null=True, blank=False)
    description= models.TextField(null=True,blank=True)
    title = models.CharField(max_length=50, null=True, blank=True,default="")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

class UserNotification(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False,unique=True)
    user_id = models.ForeignKey('User', on_delete=models.CASCADE, null=True, related_name="user_notification")
    notification_id = models.ForeignKey('Notification', on_delete=models.CASCADE, null=True)
    is_read = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

class HrContact(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False,unique=True)
    hr_id = models.ForeignKey('User', on_delete=models.CASCADE, null=True, related_name="user_hr")
    cadidate_id = models.ManyToManyField('User', related_name="user_candidate")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
