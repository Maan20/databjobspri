import json
import random
import uuid

import requests
from braces.views import CsrfExemptMixin
from django.db import transaction
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.decorators.debug import sensitive_post_parameters
from fcm_django.models import FCMDevice
from oauth2_provider.contrib.rest_framework import OAuth2Authentication, TokenHasScope
from oauth2_provider.settings import oauth2_settings
from oauth2_provider.views.mixins import OAuthLibMixin
from rest_framework import permissions, status
from rest_framework.generics import *
from rest_framework.mixins import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from twilio.rest import Client

from accounts.models import *
from accounts.serializers import *

account_sid = settings.TWILIO_ACCOUNT_SID
auth_token = settings.TWILIO_AUTH_TOKEN


def get_access_token(email, password):
    r = requests.post(
        settings.URL_TYPE + "/o/token/",
        data={
            "grant_type": "password",
            "username": email,
            "password": password,
            "client_id": settings.CLIENT_ID,
            "client_secret": settings.CLIENT_SECRET,
        },
        verify=False,
    )
    return r.json()


class UserRegister(APIView):
    permission_classes = (permissions.AllowAny,)
    server_class = oauth2_settings.OAUTH2_SERVER_CLASS
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = oauth2_settings.OAUTH2_BACKEND_CLASS

    def post(self, request):
        if request.auth is None:
            data = request.data
            serializer = RegisterSerializer(data=data)
            if serializer.is_valid():
                try:
                    user = serializer.save()
                    # response= get_access_token(serializer.data['email'],serializer.data['password'])
                    return Response(
                        data={"message": "Registration done successfully"}, status=200
                    )
                except Exception as e:
                    print(e)
                    return Response(
                        data={"error": "e"}, status=status.HTTP_400_BAD_REQUEST
                    )
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_403_FORBIDDEN)


class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        r = requests.post(
            settings.URL_TYPE + "/o/token/",
            data={
                "grant_type": "password",
                "username": request.data["mobile"],
                "password": request.data["password"],
                "client_id": settings.CLIENT_ID,
                "client_secret": settings.CLIENT_SECRET,
            },
            verify=False,
        )
        if r.status_code == 200:
            response = r.json()
            details = {}
            details.update(
                {
                    "id": user.id,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "name": user.fullname(),
                    "email": user.email,
                    "mobile": user.mobile,
                }
            )
            response.update({"details": details})
            return Response(response)
        else:
            if r.status_code == 500:
                print(r)
                return Response("r.json()", r.status_code)
            return Response(r.json(), r.status_code)


class SendOTPView(APIView):
    def post(self, request, type=None):
        serializer = SendOTPSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        if type == "hr":
            if user.role != User.HR:
                return Response({"message": "Login not allowed"}, 401)

        if user.mobile != "7865943802":
            otp = random.randint(1000, 9999)
            user.otp = otp
            user.save()
        else:
            otp = user.otp
        if user.mobile != "7865943802":
            try:
                client = Client(account_sid, auth_token)
                message = client.messages.create(
                    body="Your JOBSPRI verification code is " + str(otp),
                    from_="+13192095611",
                    to="+91" + request.data["mobile"],
                )
            except Exception as e:
                print(e)
                return Response({"message": "Please check your mobile"}, 400)

        return Response({"message": "Otp sent successfully", "otp": otp})


class OTPVerificationView(APIView):
    def post(self, request):
        serializer = OTPVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        r = requests.post(
            settings.URL_TYPE + "/o/token/",
            data={
                "grant_type": "password",
                "username": request.data["mobile"],
                "password": "OTP_VALIDATION",
                "client_id": settings.CLIENT_ID,
                "client_secret": settings.CLIENT_SECRET,
            },
            verify=False,
        )
        if r.status_code == 200:
            response = r.json()
            details = {}
            details.update(
                {
                    "id": user.id,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "name": user.fullname(),
                    "email": user.email,
                    "mobile": user.mobile,
                }
            )
            response.update({"details": details})
            return Response(response)
        else:
            if r.status_code == 500:
                print(r)
                return Response("r.json()", r.status_code)
            return Response(r.json(), r.status_code)


class UpdatePasswordView(APIView):
    def post(self, request):
        serializer = UpdatePasswordSerializer(data=request.data)
        user = User.objects.get(
            mobile=request.data["mobile"],
        )
        print(user)
        user.set_password(request.data["password"])
        user.save()
        return Response({"message": "Password updated successfully"})


class OTPVerificationForChangePasswordView(APIView):
    def post(self, request):
        serializer = OTPVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({"message": "OTP verified successfully"})


class FCMTokenUpdate(
    GenericAPIView,
    CreateModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = FCMTokenSerializer

    def post(self, request):
        is_exists = FCMDevice.objects.filter(user=request.user).exists()
        if is_exists:
            data = FCMDevice.objects.get(user=request.user)
            data.registration_id = request.data["registration_id"]
            data.save()
        else:
            data = FCMDevice.objects.create(
                registration_id=request.data["registration_id"], user=request.user
            )

        return Response({"message": "FCM token updated"})


class UserView(GenericAPIView, UpdateModelMixin):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer
    queryset = User.objects.all()
    lookup_field = "id"

    def put(self, request, id=None):
        self.update(request, self.request.user)
        return Response(
            {
                "message": "Profile updated successfully",
            }
        )
