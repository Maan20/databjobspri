from rest_framework import serializers
from accounts.models import *
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from fcm_django.models import FCMDevice
import random
import string
from rest_framework import exceptions
from django.core.mail import EmailMessage, send_mail
from smtplib import SMTPException
from django.template import loader
from django.conf import settings


class RegisterSerializer(serializers.ModelSerializer):
    def validate(self, data):
        try:
            user = User.objects.filter(mobile=data.get("mobile"))
            if len(user) > 0:
                raise serializers.ValidationError(_("Mobile already exists"))
        except User.DoesNotExist:
            pass
        return data

    class Meta:
        model = User
        fields = ("email", "first_name", "mobile")

    def create(self, validated_data):
        user = User.objects.create(
            mobile=validated_data["mobile"],
            email=validated_data["email"],
            first_name=validated_data["first_name"],
        )
        char_set = string.ascii_uppercase + string.digits
        password = "".join(random.sample(char_set * 8, 8))
        user.set_password(password)
        user.is_staff = True
        user.is_active = True
        user.role = User.EMPLOYEE
        user.save()

        data = [
            {
                "email": str(user.email),
                "first_name": str(user.first_name),
            }
        ]
        # EmailWithDynamicData("d-c21186704fd24bcb81331dff5408f05a", user.email, {})

        html_message = loader.render_to_string(
            "welcome_email.html",
            {
                "name": str(user.first_name),
            },
        )

        subject = "Welcome to jobspri"
        try:
            mail = send_mail(
                subject,
                "Hello",
                settings.EMAIL_SEND_FROM,
                [str(user.email)],
                html_message=html_message,
            )

        except:
            print(
                "EMAIL WILL BE SENT FROM ",
                settings.EMAIL_SEND_FROM,
                " BUT WE CANT SEND NOW",
            )

        return user


class LoginSerializer(serializers.Serializer):
    mobile = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        mobile = data.get("mobile", "")
        password = data.get("password", "")
        if mobile and password:
            user = User.objects.filter(mobile=mobile).first()
            if user and authenticate(mobile=mobile, password=password):
                data["user"] = user
            else:
                msg = "Unable to login with given credentials."
                raise exceptions.AuthenticationFailed({"error": msg}, code=401)
        else:
            msg = "Must provide username and password both."
            raise exceptions.ValidationError({"error": msg})
        return data


class SendOTPSerializer(serializers.Serializer):
    mobile = serializers.CharField()

    def validate(self, data):
        mobile = data.get("mobile", "")
        if mobile:
            user = User.objects.filter(mobile=mobile).first()
            if user:
                data["user"] = user
            else:
                msg = "Invalid Mobile number"
                raise exceptions.ValidationError({"error": msg})
        return data


class OTPVerificationSerializer(serializers.Serializer):
    mobile = serializers.CharField()
    otp = serializers.CharField()

    def validate(self, data):
        mobile = data.get("mobile", "")
        otp = data.get("otp", "")
        if mobile:
            user = User.objects.filter(mobile=mobile, otp=otp).first()
            if user:
                data["user"] = user
            else:
                msg = "Invalid OTP"
                raise exceptions.AuthenticationFailed({"error": msg}, code=401)
        return data


class UpdatePasswordSerializer(serializers.Serializer):
    password = serializers.CharField()
    mobile = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email", "first_name", "last_name", "mobile")


class FCMTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = FCMDevice
        fields = "registration_id"
