from django.conf.urls import url
from django.urls import path
from oauth2_provider import views as oauth2_views
from .views import *
urlpatterns = [
    path("app-version", AppVersionView.as_view()),
    path("skills", SkillsView.as_view()),
    path("courses", CourseView.as_view()),
    path("specializations", SpecializationView.as_view()),
    path("industory", IndustoryView.as_view()),
    path("interview-skills", InterviewSkillsView.as_view()),
]