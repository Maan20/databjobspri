from oauth2_provider.contrib.rest_framework import TokenHasScope, OAuth2Authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from .serializers import *
from rest_framework import generics, permissions, serializers
from rest_framework.generics import *
from rest_framework.mixins import *

class AppVersionView(APIView):
    def get(self, request, id=None):
        try:
            version = AppVersion.objects.all().order_by('-created_at')[0]
            version = int(version.version)
        except:
            version= ""
        return Response({
            "version":version
        })
        
    def post(self, request):
        app = AppVersion()
        app.version = request.data['version']
        app.save()
        return Response(
            {
                "message": "Version Updated successfully",
            }
        )



class SkillsView(GenericAPIView,ListModelMixin,):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = SkillsSerializer
    queryset = Skill.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        queryset = self.queryset
        queryset = queryset.filter(is_active=True).exclude(employee_skill__user_id=self.request.user).order_by("name")
        return queryset

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

class IndustoryView(GenericAPIView,ListModelMixin,):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = IndustorySerializer
    queryset = Industory.objects.filter(is_active=True).order_by("name")
    lookup_field = "id"

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

class CourseView(
    GenericAPIView,
    ListModelMixin,
):
    authenticat_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = AdminCourseSerializer
    queryset = Course.objects.filter(is_active=True).order_by("name")
    lookup_field = "id"

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

class SpecializationView(
    GenericAPIView,
    ListModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = SpecializationSerializer
    queryset = Specialization.objects.filter(is_active=True).order_by("name")
    lookup_field = "id"

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

class InterviewSkillsView(
    GenericAPIView,
    ListModelMixin,
):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated]
    serializer_class = InterviewSkillsSerializer
    queryset = InterviewSkills.objects.all().order_by("name")
    lookup_field = "id"

    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)
            