from rest_framework import serializers, exceptions
from accounts.models import *
from jobspri_admin.models import *

class SpecializationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Specialization
        fields = "__all__"

class AdminCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = "__all__"


class SkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = "__all__"

class IndustorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Industory
        fields = "__all__"


class JobRequirementQuestionsAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobRequirementQuestionsAnswer
        fields = "__all__"

class JobRequirementQuestionSerializer(serializers.ModelSerializer):
    questions_answer = JobRequirementQuestionsAnswerSerializer(required=False, many=True)
    class Meta:
        model = JobRequirementQuestion
        fields = "__all__"


class InterviewSkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = InterviewSkills
        fields = "__all__"