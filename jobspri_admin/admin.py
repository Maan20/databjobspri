from django.contrib import admin
from jobspri_admin.models import *
import nested_admin


class AnswerInline(nested_admin.NestedTabularInline):
    model = JobRequirementQuestionsAnswer
    extra = 4
    max_num = 4


class QuestionInline(nested_admin.NestedModelAdmin):
    model = JobRequirementQuestion
    inlines = [
        AnswerInline,
    ]
    extra = 19


class SpecializationAdmin(admin.ModelAdmin):
    search_fields = ["id", "name"]
    list_display = ("id", "name")


class CourseAdmin(admin.ModelAdmin):
    search_fields = ["id", "name"]
    list_display = ("id", "name")


class SkillAdmin(admin.ModelAdmin):
    search_fields = ["id", "name"]
    list_display = ("id", "name")


class InterviewTimeSlotAdmin(admin.ModelAdmin):
    search_fields = ["id", "time"]
    list_display = ("id", "time")


class AppVersionAdmin(admin.ModelAdmin):
    search_fields = ["id", "version"]
    list_display = ("version", "details", "created_at")


class InterviewSkillsAdmin(admin.ModelAdmin):
    search_fields = ["id", "name"]
    list_display = ("id", "name")


admin.site.register(InterviewSkills, InterviewSkillsAdmin)
admin.site.register(AppVersion, AppVersionAdmin)
admin.site.register(InterviewTimeSlot, InterviewTimeSlotAdmin)
admin.site.register(JobRequirementQuestion, QuestionInline)
admin.site.register(Specialization, SpecializationAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Skill, SkillAdmin)
admin.site.register(Industory)
admin.site.register(Department)
